import gql from 'graphql-tag'

const likeComment = gql`
mutation ($commentId: Int) {
  likeComment (commentId: $commentId) {
    result
  }
}`

const sendComment = gql`
mutation Comment($photoId: ID, $body: String) {
  comment(photoId: $photoId, body: $body) {
    comment {
      id,
      date,
      description,
      author {
        id,
        username,
        firstName,
        lastName
      }
    }
  }
}`

export {likeComment, sendComment}
