// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import VueApollo from 'vue-apollo'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'

import {LIcon, LMap, LMarker, LTileLayer} from 'vue2-leaflet'
import Vue2LeafletMarkerCluster from 'vue2-leaflet-markercluster'
import {Icon} from 'leaflet'
import 'leaflet/dist/leaflet.css'
import 'leaflet.markercluster/dist/MarkerCluster.css'
import 'leaflet.markercluster/dist/MarkerCluster.Default.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import apolloClient from './apollo-client.js'
import VueCookie from 'vue-cookie'

import {HttpLink} from 'apollo-link-http'

Vue.config.productionTip = false

const opts = {
  credentials: 'include',
  uri: 'http://localhost:8000/graphql'
}

// const httpLink = new HttpLink(opts)

// const errorLink = onError(({graphQLErrors}) => {
//   if (graphQLErrors && graphQLErrors.length > 0) {
//     rootVue.$children[0].$refs.alarms.push(graphQLErrors[0].message)
//   }
// })
//
// const csrfMiddleware = new ApolloLink((operation, forward) => {
//   // Добавляем CSRF-токен к запросу
//   operation.setContext({
//     headers: {
//       'X-CSRFToken': Cookie.get('csrftoken') || null
//     }
//   })
//   return forward(operation)
// })

// const link = errorLink.concat(concat(csrfMiddleware, httpLink))

// const apolloClient = new ApolloClient({
//   link: httpLink,
//   cache: new InMemoryCache(),
//   connectToDevTools: true,
//   fetchOptions: {
//     mode: 'no-cors'
//   }
// })
// const apolloClient = new ApolloClient({
//     link: link,
//     cache: new InMemoryCache()
// })

Vue.use(VueApollo)
Vue.use(Vuetify, {
  iconfont: 'md'
})
Vue.use(Vuetify)
Vue.use(VueCookie)

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
    $loadingKey: 'loading'
  }
})

Vue.component('l-map', LMap)
Vue.component('l-tile-layer', LTileLayer)
Vue.component('l-marker', LMarker)
Vue.component('l-icon', LIcon)
Vue.component('l-marker-cluster', Vue2LeafletMarkerCluster)

delete Icon.Default.prototype._getIconUrl

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
})

/* eslint-disable no-new */
export default new Vue({
  el: '#app',
  provide: apolloProvider.provide(),
  router,
  components: { App },
  template: '<App/>'
})
