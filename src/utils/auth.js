import Vue from 'vue'
import gql from 'graphql-tag'

const auth = new Vue({
  data: {
    client: null,
    user: {
      id: 0,
      username: '',
      isSuperuser: false,
    }
  },
  methods: {
    async loggedIn () {
      console.log('хуй')
      try {
        let {data} = await this.$apollo.query({
          query: gql`
            query {
              currentUser {
                id
                username
              }
            }
          `
        })
        if (data.currentUser) {
          this.user.id = data.currentUser.id
          this.user.username = data.currentUser.username
        }
        return data.currentUser !== null
      } catch (e) {
        throw new Error(e)
        return false
      }
    },
    async login (name, password, commonComputer) {
      try {
        let {data} = await this.client.mutate({
          mutation: gql`
            mutation ($params: LoginInput!) {
              login(input: $params) {
                success,
                user {
                  id
                  username
                }
              }
          }`,
          variables: {
            params: {
              login: name,
              password: password,
              commonComputer: commonComputer
            }
          }
        })
        if (data.login.success) {
          this.user.id = data.login.user.id
          this.user.isSuperuser = data.login.user.isSuperuser
          this.user.username = data.login.user.username
        } else {
          this.user.id = 0
          this.user.isSuperuser = false
          this.user.username = ''
        }
        return data.login.success
      } catch (e) {
        return false
      }
    },
    async logout () {
      if (this.loggedIn()) {
        try {
          let {data} = await this.$apollo.mutate({
            mutation: gql`
              mutation {
                logout {
                  success
                }
              }`
          })
          if (data.logout.success) {
            this.user.id = 0
            this.user.username = ''
          }
          return data.logout.success
        } catch (e) {
          return false
        }
      } else {
        return false
      }
    },
    registered (newUser) {
      this.user.id = newUser.id
      this.user.username = newUser.username
    }
  }
})

export default auth
