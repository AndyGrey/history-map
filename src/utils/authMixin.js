import gql from 'graphql-tag'
import moment from 'moment'

const authMixin = {
  mounted () {
    this.getCurrentUser()
  },
  data () {
    return {
      currentUser: {
        id: 0,
        username: null
      },
      isUserLogged: false
    }
  },
  methods: {
    formatDate (textDate) {
      if (textDate) {
        return moment(textDate).format('L')
      } else {
        return ''
      }
    },
    getCurrentUser () {
      console.log(123)
      this.$apollo.query({
        query: gql`{
          currentUser {
            id
            username
          }
        }`
      }).then(({data}) => {
        console.log('THIS USER')
        console.log(data)
        if (data.currentUser) {
          this.currentUser.id = data.currentUser.id
          this.currentUser.username = data.currentUser.username
          this.isUserLogged = true
        }
      })
    }
  }
}

export default authMixin
