export class BackendUtils {
  static absolute (url) {
    let base = process.env.VUE_APP_API_STATIC_URL
    base = base.substring(0, base.length - 1)
    return base + url
  }
}
