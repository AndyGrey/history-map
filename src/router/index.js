import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Map from '@/components/Map'
import Register from '@/components/login/Register'
import Login from '@/components/login/Login'
import page404 from '@/components/page404'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/map',
      name: 'Map',
      component: Map
    },
    {
      path: '/map/photos/:photoId',
      name: 'MapPhoto',
      component: Map
    },
    {
      path: '/register',
      name: 'registration',
      component: Register
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '*',
      name: 'page404',
      component: page404
    }
  ]
})

router.beforeEach((to, from, next) => {
  console.log(to)
  next()
})

export default router
