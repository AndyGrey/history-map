"""cookbook URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static

from django.conf import settings

from graphene_django.views import GraphQLView
from cookbook.schema import schema


from django.views.decorators.csrf import csrf_exempt


def _csrf_exempt(view_func):
    """
      Функция оборачивает GraphQL API в вызов csrf_exempt,
      который позволяет избавиться от ошибок при запросах с другого хоста (в режиме разработки).
      На продакшене функция возвращает необернутый view_func, чтобы обеспечить защиту от CSRF.
      CSRF - https://en.wikipedia.org/wiki/Cross-site_request_forgery
    """
    if settings.DEBUG:
        return csrf_exempt(view_func)
    else:
        return view_func


urlpatterns = [
    path('admin/', admin.site.urls),
    path('graphql', _csrf_exempt(GraphQLView.as_view(graphiql=True, schema=schema)))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
