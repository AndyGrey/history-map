from django.contrib import admin
from cookbook.ingredients import models

# Register your models here.
admin.site.register(models.Location)
admin.site.register(models.DateOrPeriod)
admin.site.register(models.Planet)
admin.site.register(models.Event)
admin.site.register(models.GeoPhoto)
admin.site.register(models.Comment)
admin.site.register(models.Like)
