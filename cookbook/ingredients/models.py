from django.db import models


class Location(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()


class DateOrPeriod(models.Model):
    from_date = models.DateField(default=None, blank=True, null=True)
    to_date = models.DateField(default=None, blank=True, null=True)
    date = models.DateField(default=None, blank=True, null=True)


class Planet(models.Model):
    name = models.CharField(max_length=100)
    preview_image = models.CharField(max_length=1024, default='')
    sort_number = models.IntegerField(default=0)


class Event(models.Model):
    location = models.ForeignKey(Location, on_delete=models.PROTECT)
    title = models.CharField(max_length=100)
    description = models.TextField()
    date = models.ForeignKey(DateOrPeriod, on_delete=models.PROTECT)
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT)


class GeoPhoto(models.Model):
    url = models.CharField(max_length=2048)
    location = models.ForeignKey(Location, on_delete=models.PROTECT)
    title = models.CharField(max_length=100)
    description = models.TextField()
    date = models.ForeignKey(DateOrPeriod, on_delete=models.PROTECT)
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT)


class GeoPhotoLike(models.Model):
    user = models.ForeignKey('auth.User', verbose_name='Пользователь', on_delete=models.PROTECT)
    geo_photo = models.ForeignKey(GeoPhoto, on_delete=models.PROTECT)


class Comment(models.Model):
    date = models.DateTimeField(verbose_name='Дата комментария', auto_now_add=True)
    description = models.CharField(max_length=256)
    author = models.ForeignKey('auth.User', verbose_name='Пользователь', on_delete=models.PROTECT)
    geo_photo = models.ForeignKey(GeoPhoto, on_delete=models.PROTECT)


class Like(models.Model):
    user = models.ForeignKey('auth.User', verbose_name='Пользователь', on_delete=models.PROTECT)
    comment = models.ForeignKey(Comment, on_delete=models.PROTECT)
