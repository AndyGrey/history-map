import os
import datetime
import graphene
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from graphene_django.types import DjangoObjectType

from graphene_django.converter import convert_django_field
from cookbook.ingredients import models
from PIL import Image


class Period(graphene.ObjectType):
    from_date = graphene.Date(required=True)
    to_date = graphene.Date(required=True)


class PeriodInput(graphene.InputObjectType):
    from_date = graphene.Date(required=True)
    to_date = graphene.Date(required=True)


class DateOrPeriod(graphene.Union):
    class Meta:
        types = (graphene.Date(), Period())


class GeoPhotoFilterParamsInput(graphene.InputObjectType):
    take = graphene.Int()
    date = graphene.Date()
    period = PeriodInput()

    def get_query(self, where=None, params=None):
        query = models.GeoPhoto.objects
        if where:
            query = query.extra(
                where=where, params=params)

        if self.date:
            where.append('date_or_period.date=%o')
            params.append(self.date)
            query = query.filter(date__date=self.date)
        elif self.period:
            query = query.filter(date__from_date=self.period.from_date)
            query = query.filter(date__to_date=self.period.to_date)

        query = query.order_by('id')

        if self.take:
            return query[:self.take]
        return query


class EventFilterParams(graphene.ObjectType):
    planet_id = graphene.ID()
    date = DateOrPeriod()


class EventType(DjangoObjectType):
    class Meta:
        model = models.Event


class DateOrPeriodType(DjangoObjectType):
    class Meta:
        model = models.DateOrPeriod


class CommentType(DjangoObjectType):
    class Meta:
        model = models.Comment


class LikeType(DjangoObjectType):
    class Meta:
        model = models.Like


class LikePhotoType(DjangoObjectType):
    class Meta:
        model = models.GeoPhotoLike


class GeoPhotoType(DjangoObjectType):
    class Meta:
        model = models.GeoPhoto
    url_small = graphene.String()

    def resolve_url_small(self, info):
        filename, file_extension = os.path.splitext(self.url)
        filename = filename[filename.rfind('/') + 1:]
        return '/static/uploads/small/' + filename + file_extension


class LocationType(DjangoObjectType):
    class Meta:
        model = models.Location
        exclude_fields = ('id')


class PlanetType(DjangoObjectType):
    class Meta:
        model = models.Planet
        exclude_fields = ('geophoto_set', 'event_set')

    geo_photos = graphene.Field(
        graphene.List(graphene.NonNull(GeoPhotoType), required=True),
        params=GeoPhotoFilterParamsInput(required=True))

    def resolve_geo_photos(self, info, params):
        return params.get_query(["planet_id=%s"], [self.id])


class UserType(DjangoObjectType):

    class Meta:
        model = User


@convert_django_field.register(models.DateOrPeriod)
def convert_field(field, registry=None):
    return DateOrPeriod()


def resize_image(input_image_path, output_image_path, size):
    original_image = Image.open(input_image_path)
    resized_image = original_image.resize(size)
    resized_image.save(output_image_path)


class Query(object):
    planets = graphene.List(graphene.NonNull(PlanetType), required=True)
    planet = graphene.Field(PlanetType, id=graphene.ID(required=True))
    geo_photo = graphene.Field(
        GeoPhotoType,
        id=graphene.ID(required=True))
    geo_photos = graphene.Field(
        graphene.List(graphene.NonNull(GeoPhotoType), required=True),
        params=GeoPhotoFilterParamsInput(required=True))
    current_user = graphene.Field(UserType, description='Базовая информация о залогиненом пользователе')


    def resolve_planets(self, info):
        return models.Planet.objects.order_by('sort_number').all()

    def resolve_planet(self, info, id):
        return models.Planet.objects.get(pk=id)

    def resolve_geo_photo(self, info, id):
        return models.GeoPhoto.objects.get(pk=id)

    def resolve_geo_photos(self, info, params):
        return params.get_query()

    def resolve_current_user(self, info):
        print(info.context.user)
        if info.context.user.is_authenticated:
            return info.context.user
        else:
            return None


class SendPhoto(graphene.Mutation):
    class Input:
        x = graphene.Float(required=True)
        y = graphene.Float(required=True)
        description_photo = graphene.String(required=True)
        name = graphene.String(required=True)
        date = graphene.String()
        start_date = graphene.String()
        end_date = graphene.String()

    result = graphene.Boolean()

    def mutate(self, info, **kwargs):
        location = models.Location.objects.create(latitude=kwargs['x'], longitude=kwargs['y'])
        if kwargs.get('date'):
            date = models.DateOrPeriod.objects.create(date=kwargs['date'])
        else:
            date = models.DateOrPeriod.objects.create(from_date=kwargs['start_date'], to_date=kwargs['end_date'])

        photo = models.GeoPhoto.objects.create(
            location=location,
            title=kwargs['name'],
            description=kwargs['description_photo'],
            date=date,
            planet_id=1
        )
        file_format = str(info.context.FILES['file']).split('.')[-1]
        models.GeoPhoto.objects.filter(id=photo.id).update(url='/static/uploads/{0}.{1}'.format(photo.id, file_format))

        file_data = info.context.FILES['file'].read()
        file = open('cookbook/ingredients/static/uploads/{0}.{1}'.format(photo.id, file_format), 'wb')
        file.write(file_data)
        file.close()
        resize_image(
            'cookbook/ingredients/static/uploads/{0}.{1}'.format(photo.id, file_format),
            'cookbook/ingredients/static/uploads/small/{0}.{1}'.format(photo.id, file_format),
            (64, 64)
        )
        return SendPhoto(result=True)


class Login(graphene.Mutation):
    class Meta:
        description = 'Вход пользователя в систему'

    class Input:
        username = graphene.String(required=True, description='Логин пользователя')
        password = graphene.String(required=True, description='Пароль пользователя')

    user = graphene.Field(UserType, description='Информация о залогиненом пользователе')
    success = graphene.Boolean(required=True, description='Успех операции')

    @staticmethod
    def mutate(root, info, **kwargs):
        # user = authenticate(info.context, **kwargs)
        user = User.objects.get(username=kwargs['username'])
        user.check_password(kwargs['password'])
        if user is not None:
            login(info.context, user)
        return Login(user=user, success=user is not None)


class Logout(graphene.Mutation):
    class Meta:
        description = 'Выход пользователя из системы'

    success = graphene.Boolean(required=True, description='Успех операции')

    @staticmethod
    def mutate(root, info):
        if info.context.user.is_authenticated:
            logout(info.context)
            return Logout(success=True)
        else:
            return Logout(success=False)


class Register(graphene.Mutation):
    new_user = graphene.Field(UserType)
    logged_in = graphene.Boolean()

    class Input:
        username = graphene.String()
        password = graphene.String()

    def mutate(self, info, **kwargs):
        new_user = User.objects.create_user(**kwargs)
        user = authenticate(info.context, **kwargs)
        login(info.context, user)
        return Register(new_user=new_user, logged_in=True)


class LikeComment(graphene.Mutation):
    result = graphene.Boolean()

    class Input:
        comment_id = graphene.Int()

    def mutate(self, info, comment_id):
        like_users = models.Like.objects.filter(comment_id=comment_id)
        print(comment_id, info.context.user.id)
        for user in like_users.all():
            if user.user.id == info.context.user.id:
                user.delete()
                return LikeComment(result=True)
        models.Like.objects.create(user_id=info.context.user.id, comment_id=comment_id)

        return LikeComment(result=True)


class LikePhoto(graphene.Mutation):
    result = graphene.Boolean()

    class Input:
        photo_id = graphene.Int()

    def mutate(self, info, photo_id):
        like_photos = models.GeoPhotoLike.objects.filter(geo_photo_id=photo_id)
        for user in like_photos.all():
            if user.user.id == 1:
                user.delete()
                return LikePhoto(result=True)
        models.GeoPhotoLike.objects.create(user_id=1, geo_photo_id=photo_id)

        return LikePhoto(result=True)


class Comment(graphene.Mutation):
    comment = graphene.Field(CommentType)

    class Input:
        photo_id = graphene.ID()
        body = graphene.String()

    def mutate(self, info, photo_id, body):
        c = models.Comment.objects.create(
                date=datetime.datetime.now(),
                geo_photo_id = photo_id,
                author_id = info.context.user.id,
                description=body)
        return Comment(comment=c)


class Mutation(graphene.ObjectType):
    login = Login.Field()
    logout = Logout.Field()
    register = Register.Field()
    like_comment = LikeComment.Field()
    like_photo = LikePhoto.Field()
    comment = Comment.Field()
    send_photo = SendPhoto.Field()
