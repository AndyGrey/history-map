from PIL import Image
from os import listdir
from os.path import isfile, join


class _Image(Image.Image):
    def crop_to_aspect(self, aspect, divisor=1, alignx=0.5, aligny=0.5):
        """Crops an image to a given aspect ratio.
        Args:
            aspect (float): The desired aspect ratio.
            divisor (float): Optional divisor. Allows passing in (w, h)
                             pair as the first two arguments.
            alignx (float): Horizontal crop alignment from
                            0 (left) to 1 (right)
            aligny (float): Vertical crop alignment from 0 (left) to 1 (right)
        Returns:
            Image: The cropped Image object.
        """
        if self.width / self.height > aspect / divisor:
            newwidth = int(self.height * (aspect / divisor))
            newheight = self.height
        else:
            newwidth = self.width
            newheight = int(self.width / (aspect / divisor))
        img = self.crop((alignx * (self.width - newwidth),
                         aligny * (self.height - newheight),
                         alignx * (self.width - newwidth) + newwidth,
                         aligny * (self.height - newheight) + newheight))
        return img


Image.Image.crop_to_aspect = _Image.crop_to_aspect

d = 'C:/Dev/history-map/'
in_path = join(d, 'cookbook/ingredients/static/uploads')
out_path = join(d, 'cookbook/ingredients/static/uploads/small')

files = [f for f in listdir(in_path) if isfile(join(in_path, f))]
for infile in files:
    outfile = join(out_path, infile)
    if infile != outfile:
        try:
            w = 64
            h = 64
            im = Image.open(join(in_path, infile))
            cropped = im.crop_to_aspect(w, h)
            cropped.thumbnail((w, h), Image.ANTIALIAS)
            cropped.save(outfile, "JPEG")
        except IOError:
            print("cannot create thumbnail for '{}'".format(infile))
